# Nuxt 3 Boilerplate

A nuxt 3 boilerplate for further use.

## Overview

This boilerplate includes the following technologies / packages:

- nuxt 3 (rc.12 right now)
- tailwind 3
- vueuse

### Development

- eslint
- stylelint
- husky
- lint-staged
- cypress

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000

```bash
# yarn
yarn dev

# npm
npm run dev
```

## Production

Build the application for production:

```bash
# yarn
yarn build

# npm
npm run build
```

Locally preview production build:

```bash
# yarn
yarn preview

# npm
npm run preview
```
