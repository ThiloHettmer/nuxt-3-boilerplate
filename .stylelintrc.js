module.exports = {
  extends: ['stylelint-config-recommended-vue'],
  ignoreFiles: ['node_modules/**/*', '.nuxt/**/*'],
  rules: {
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: ['extends', 'tailwind', 'apply', 'variants', 'responsive', 'screen'],
      },
    ],
  },
}
