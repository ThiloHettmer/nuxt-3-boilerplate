import { defineNuxtConfig } from 'nuxt/config'
import eslintPlugin from 'vite-plugin-eslint'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  build: {
    transpile: ['@headlessui/vue', '@heroicons/vue'],
  },
  css: [
    '~/assets/css/tailwind.css',
  ],
  modules: [
    '@vueuse/nuxt',
    '@nuxtjs/stylelint-module',
  ],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    }
  },
  tailwindcss: {
    mode: 'jit',
    configPath: 'tailwind.config.js',
    viewer: false,
    exposeConfig: false,
  },
  vite: {
    plugins: [eslintPlugin()],
    ssr: {
      noExternal: ['@headlessui/vue'],
    },
    server: {
      watch: {
        usePolling: true,
      },
    },

  },
  components: true
})
